
function getSectionList(story, context, resolve) {
    const ROOT_SECTION_ENDPOINT = `172.22.211.68:8080/webservice/escenic/section/search?q&c=300`;
    let sectionList = [];
    
    context.cook.get(ROOT_SECTION_ENDPOINT, {}, response => {
        for (let i = 0; i < response.entity.data.length; i++) {
            let currentItem = response.entity.data[i];
            if (Array.isArray(currentItem) && currentItem[0] === 'atom:entry' && currentItem[8][1].title==='tomorrow-online') {
                sectionList.push(currentItem[2][1]);
            }
            resolve(sectionList);
        }
    });
    
}

exports.assemble = function (assembler) {
    assembler.add(
        `
            extend type Resolution {
               sectionList: [String]
            }
            
        `,
        {
            Resolution: {
                sectionList: (story,args,context) => {
                    return new Promise((resolve) => {
                        getSectionList(story, context,resolve);
                    });
                }
            },
        }
    );
};
